from fractions import Fraction
import math
import numpy as np


class Matrix:
    def __init__(self, data):
        self.data = np.array(data, dtype=object)

    @classmethod
    def fromstr(cls, data_string):
        data = [[int(x) if '/' not in x else Fraction(x) for x in row.split()]
                for row in data_string.split('\n')]
        return Matrix(data)

    def simplex(self):
        """
        1. Identify entering basic variable
        2. Identify leaving basic variable
        3. Perform pivots
        """
        # cannot be 0 or n-1
        entering_basic_variable = 0

        # cannot be n-1
        leaving_basic_variable = 0

        # determine pivots
        pass

    def transform(self, r1_operation, r2_operation, target_row):
        r1_operation = RowOperation.fromstr(r1_operation)
        r2_operation = RowOperation.fromstr(r2_operation)
        r3_operation = RowOperation.fromstr(target_row)

        r1 = self.get_row(r1_operation.index) * r1_operation.multiplier
        r2 = self.get_row(r2_operation.index) * r2_operation.multiplier

        new_row = r1 + r2

        self.set_row(r3_operation.index, new_row)

    def get_row(self, row_index):
        return self.data[row_index]

    def set_row(self, row_index, new_row):
        self.data[row_index] = new_row

    def get_col(self, col_index):
        return self.data[:, col_index]

    def __eq__(self, other):
        return (self.data == other.data).all

    def __str__(self):
        return "\n".join(" ".join(str(cell) for cell in row) for row in self.data)


class RowOperation:
    def __init__(self, index, multiplier):
        # map from 1 index to 0 index
        self.index = index - 1
        self.multiplier = multiplier

    @classmethod
    def fromstr(cls, data):
        multiplier = extract_multiplier(data)
        index = extract_index(data)
        return RowOperation(index, multiplier)


def extract_index(row_identifier):
    index = row_identifier.split('R')[1]
    return int(index)


def extract_multiplier(row_identifier):
    multiplier = row_identifier.split('R')[0]
    if multiplier == '':
        return 1
    if multiplier == '-':
        return -1

    if '/' in multiplier:
        print(multiplier)
        return Fraction(multiplier)
    return int(multiplier)


def LCM(a, b):
    return abs(a*b) // math.gcd(a, b)
