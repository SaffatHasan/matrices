from matrix import Matrix

m = Matrix.fromstr("""0  1 -1 0 1 0 0 2
    0  2  0 1 0 1 0 4
    0  1  1 1 0 0 1 3
    1 -2  0 -3/2 0 0 0 0""")
print("T1=")
print(m)

# T1 % show T1

# disp('Pivoting on R1 -> x1');
# T2 = T1;
# T2 = row_operation(T2, '-2R1', 'R2', 'R2');
m.transform('-2R1', 'R2', 'R2')
print("\nT2=")
print(m)
# T2 = row_operation(T2, '-1R1', 'R3', 'R3');
# T2 = row_operation(T2, '2R1', 'R4', 'R4');
# T2 % show T2

# disp('Pivot on R3 -> x2');
# T3 = T2;
# T3 = row_operation(T3, '2R1', 'R3', 'R1');
# T3 = row_operation(T3, 'R2', '-1R3', 'R2');
# T3 = row_operation(T3, 'R3', 'R4', 'R4');
# T3 = row_operation(T3, '0.5R3', '0R3', 'R3');
# T3 % show T3

# disp('Pivot on R3 -> x3');
# T4 = T3;
# T4 = row_operation(T4, 'R1', '-2R3', 'R1');
# T4 = row_operation(T4, 'R3', 'R4', 'R4');
# T4 = row_operation(T4, '2R3', '0R3', 'R3');
# T4 = row_operation(T4, '0.5R1', '0R1', 'R1');
# % T4 = row_operation(T4, '0.5R1', '-1R3', 'R3');
# % T4 = row_operation(T4, '0.5R1', 'R4', 'R4');
# T4 % show T4
