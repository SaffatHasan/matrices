# Matrices

This project is intended to allow a text based tool to perform operations such as:

- m = Matrix.fromstr('''5 4 1 2
  1 2 5 2
  0 0 0 0''')
- m.transform('R1 + R2 -> R3')
- m.simplex() -> perform one step of simplex method
