from src.matrix import Matrix, extract_multiplier, extract_index
import numpy as np
from fractions import Fraction
import unittest


class TestMatrix(unittest.TestCase):

    def test_fromstr(self):
        actual = Matrix.fromstr('''1 2 3 4
        5 6 7 8''')

        assert (actual.data == np.array([[1, 2, 3, 4], [5, 6, 7, 8]])).all()

    # def test_fromstr_fractions(self):
    #     actual = Matrix.fromstr('''-19/3 0 1/3 0
    #     10/3 0 1/3''')

    #     assert actual.data == [[]]

    # def test_parse_string(self):
        # actual = parse('''R1 + R2 -> R3''')

    def test_extract_multiplier(self):
        assert extract_multiplier('R1') == 1
        assert extract_multiplier('-R1') == -1
        assert extract_multiplier('2R2') == 2
        assert extract_multiplier('-4R1') == -4
        assert extract_multiplier('1/3R1') == Fraction(1, 3)

    def test_extract_row_index(self):
        assert extract_index('R1') == 1
        assert extract_index('-R1') == 1
        assert extract_index('2R2') == 2
        assert extract_index('-4R1') == 1
        assert extract_index('1/3R1') == 1

    def test_get_row(self):
        m = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        assert all(m.get_row(0) == [1, 2, 3])
        assert all(m.get_row(1) == [4, 5, 6])
        assert all(m.get_row(2) == [7, 8, 9])

    def test_get_col(self):
        m = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        assert all(m.get_col(0) == [1, 4, 7])
        assert all(m.get_col(1) == [2, 5, 8])
        assert all(m.get_col(2) == [3, 6, 9])

    def test_transform(self):
        m = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        m.transform('R1', 'R2', 'R3')
        expected = Matrix([[1, 2, 3], [4, 5, 6], [5, 7, 9]])
        assert m == expected

    def test_transform_2(self):
        m = Matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        m.transform('1/3R1', '3R2', 'R3')

        expected = Matrix([[1, 0, 0], [0, 1, 0], [Fraction(1/3), 3, 1]])

        assert m == expected

    def test_to_string(self):
        m = Matrix.fromstr('''-19/3 0 1/3 0
        10/3 0 1/3''')

        assert str(m) == "-19/3 0.0 1/3 0.0\n10/3 0.0 1/3"
